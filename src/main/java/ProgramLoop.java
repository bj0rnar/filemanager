import filehandling.FileExtractor;
import filehandling.FileInspector;
import init.Initializer;
import model.Sets;
import utility.CustomLogger;
import inputvalidation.MenuInputValidator;
import utility.ListSearcher;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;
import java.util.Scanner;


/**
 * TODO:
 * Main loop for user interaction
 * First requirement - list all files DONE
 * Second requirement - find files based on extensions DONE
 * Third requirement - Dracula text:
 * - Count number of lines DONE
 * - Retrieve name DONE
 * - Retrieve file size DONE
 * - checkup on word
 * - optimized DONE
 * - unoptimized DONE
 * - frequency of specific word
 * - optimized DONE
 * - unoptimized DONE
 * - refactor
 * - add comments
 */


public class ProgramLoop {

    /**
     * Class for holding main loop and entry point for the program.
     */

    public static void initializeProgramLoop() {
        boolean running = true;

        Scanner scanner = new Scanner(System.in);

        //Sets returns formatted hashsets, arraylists and hashmap of the Dracula.txt file, used for the word search and word frequency check
        Sets wordSearchSets = Initializer.searchForWordInit();
        Sets wordFrequencySets = Initializer.wordFrequencyInit();

        //StartTime and StopTime are used for time measuring of all methods so they can be logged.
        long startTime;
        long stopTime;


        while (running) {
            System.out.println("1: List all files");
            System.out.println("2: Find files based on extension");
            System.out.println("3: Checkout the dracula file");
            System.out.println("4: Terminate the program");
            System.out.println("Select your option: ");

            String userChoice = scanner.nextLine();

            if (MenuInputValidator.checkIfUserEnteredValidMenuOption(userChoice, 4)) {

                //Can parse now that we know it's safe
                int userSelection = Integer.parseInt(userChoice);

                if (userSelection == 1) {
                    startTime = System.nanoTime();
                    FileExtractor fileExtractor = FileExtractor.getInstance();
                    File[] allFiles = fileExtractor.getAllFiles();

                    for (File f : allFiles) {
                        System.out.print(FileInspector.retrieveNameOfFile(f) + " ");
                    }
                    System.out.println();
                    stopTime = System.nanoTime();
                    //Get current instance of Singleton Class and log the action
                    CustomLogger.getInstance().logUserAction("retrieved all the files", (stopTime - startTime));
                }

                if (userSelection == 2) {
                    System.out.println("Input an extension: ");
                    String extensionInput = scanner.nextLine();

                    if (MenuInputValidator.checkIfSearchWordIsValid(extensionInput)) {
                        startTime = System.nanoTime();
                        FileExtractor fileExtractor = FileExtractor.getInstance();
                        ArrayList<File> extensionFiles = fileExtractor.getFilesByExtension(extensionInput);

                        for (File f : extensionFiles) {
                            System.out.print(FileInspector.retrieveNameOfFile(f) + " ");
                        }

                        System.out.println();
                        stopTime = System.nanoTime();
                        CustomLogger.getInstance().logUserAction("searched for " + extensionInput + "extension and found " + extensionFiles, (stopTime - startTime));

                    } else {
                        System.out.println("Invalid input");
                    }
                }

                if (userSelection == 3) {
                    //Since we are only manipulating one file, retrieve it before starting the loop
                    FileExtractor f = FileExtractor.getInstance();
                    File dracula = f.getSpecificFileByName("Dracula.txt");

                    boolean draculaLoop = true;
                    while (draculaLoop) {
                        System.out.println("1: Count number of lines");
                        System.out.println("2: Get the name of the file");
                        System.out.println("3: Check out the file size");
                        System.out.println("4: Check if word is in text");
                        System.out.println("5: Find occurences of word");
                        System.out.println("6: Go back");

                        String draculaChoice = scanner.nextLine();
                        if (MenuInputValidator.checkIfUserEnteredValidMenuOption(draculaChoice, 6)) {

                            int draculaSelection = Integer.parseInt(draculaChoice);
                            if (draculaSelection == 1) {
                                startTime = System.nanoTime();
                                System.out.println("Total number of lines: " + FileInspector.countNumberOfLinesInFile(dracula));
                                stopTime = System.nanoTime();
                                CustomLogger.getInstance().logUserAction("inspected the total number of lines in Dracula.txt", (stopTime - startTime));

                            }

                            if (draculaSelection == 2){
                                startTime = System.nanoTime();
                                System.out.println("Name of the file: " + FileInspector.retrieveNameOfFile(dracula));
                                stopTime = System.nanoTime();
                                CustomLogger.getInstance().logUserAction("inspected the file name of Dracula.txt", (stopTime - startTime));
                            }

                            if (draculaSelection == 3){
                                startTime = System.nanoTime();
                                System.out.println("The size of the file is: " + FileInspector.checkFileSize(dracula));
                                stopTime = System.nanoTime();
                                CustomLogger.getInstance().logUserAction("inspected the file size of Dracula.txt", (stopTime - startTime));
                            }

                            if (draculaSelection == 4){
                                System.out.println("Enter searchword: ");
                                String searchWord = scanner.nextLine();
                                if (MenuInputValidator.checkIfSearchWordIsValid(searchWord)){

                                    long linkedHashSetStartTime = System.nanoTime();
                                    boolean linkedHashSetFound = ListSearcher.unsortedLinkedHashSetSearch(wordSearchSets.getLinkedHashSet(), searchWord.toLowerCase());
                                    long linkedHashSetStopTime = System.nanoTime();
                                    System.out.println((linkedHashSetFound ? "LinkedHashSet found \"" : "LinkedHashSet didn't find \"") + searchWord  + "\" in the text. Checking took " + (linkedHashSetStopTime-linkedHashSetStartTime) + " nanoseconds");
                                    CustomLogger.getInstance().logUserAction("used LinkedHashSet search to look for \"" + searchWord + "\" and " + (linkedHashSetFound?"found":"didn't find") + " it", (linkedHashSetStopTime - linkedHashSetStartTime));


                                    long arrayListStartTime = System.nanoTime();
                                    boolean arrayListFound = ListSearcher.naiveWordSearch(wordSearchSets.getArrayList(), searchWord.toLowerCase());
                                    long arrayListStopTime = System.nanoTime();
                                    System.out.println((arrayListFound ? "ArrayList found \"" : "ArrayList didn't find \"")  + searchWord + "\" in the text. Checking took " + (arrayListStopTime-arrayListStartTime) + " nanoseconds");
                                    CustomLogger.getInstance().logUserAction("used ArrayList search to look for \"" + searchWord + "\" and " + (arrayListFound?"found":"didn't find") + " it", (arrayListStopTime - arrayListStartTime));
                                }

                            }


                            if (draculaSelection == 5){
                                System.out.println("Enter searchword: ");
                                String searchWord = scanner.nextLine();

                                if (MenuInputValidator.checkIfSearchWordIsValid(searchWord)){

                                    long arrayStartTime = System.nanoTime();
                                    int arrayListFrequency = ListSearcher.frequencyNaive(wordFrequencySets.getArrayList(), searchWord);
                                    long arrayStopTime = System.nanoTime();
                                    System.out.println("ArrayList found " + arrayListFrequency + " instances of: " + "\"" + searchWord + "\"" + ". It took " + (arrayStopTime-arrayStartTime) + " nanoseconds");
                                    CustomLogger.getInstance().logUserAction("used ArrayList search to look for occurences of \"" + searchWord + "\" and found " + arrayListFrequency + " occurrences", arrayStopTime-arrayStartTime);


                                    long hashMapStartTime = System.nanoTime();
                                    Integer hashMapFrequency = ListSearcher.frequencyHashMap(wordFrequencySets.getHashMap(), searchWord);
                                    long hashMapStopTime = System.nanoTime();

                                    System.out.println("Hashmap found " + Objects.requireNonNullElse(hashMapFrequency, 0) + " instances of: " + "\"" + searchWord + "\" : " + ". It took " + (hashMapStopTime - hashMapStartTime) + " nanoseconds");
                                    CustomLogger.getInstance().logUserAction("used HashMap search to look for occurences of \"" + searchWord + "\" and found " + Objects.requireNonNullElse(hashMapFrequency, 0) + " occurrences", hashMapStopTime-hashMapStartTime);

                                }
                            }

                            if (draculaSelection == 6) {
                                draculaLoop = false;
                            }
                        }
                        else {
                            System.out.println("Invalid input");
                            CustomLogger.getInstance().logUserAction("entered an invalid menu choice" + draculaChoice, 0);
                        }

                    }
                }

                if (userSelection == 4) {
                    System.out.println("Bye");
                    CustomLogger.getInstance().logUserAction("exited the program", 0);
                    running = false;
                }


            } else {
                System.out.println("Invalid choice, try again");
                CustomLogger.getInstance().logUserAction("entered an invalid menu choice" + userChoice, 0);
            }

        }

        //Close stream
        CustomLogger.getInstance().cleanUp();
    }

    public static void main(String[] args) {
        initializeProgramLoop();
    }
}
