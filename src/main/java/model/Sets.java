package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;

public class Sets {

    /**
     * Custom class for holding two lists, used for the initializer process.
     * Two different constructors because there are two types of arraylists
     */


    private LinkedHashSet<String> linkedHashSet;
    private ArrayList<String> arrayList;
    private HashMap<String, Integer> hashMap;

    public Sets(ArrayList<String> arrayList, LinkedHashSet<String> hashSet){
        this.linkedHashSet = hashSet;
        this.arrayList = arrayList;
    }

    public Sets(ArrayList<String> arrayList, HashMap<String, Integer> hashMap){
        this.arrayList = arrayList;
        this.hashMap = hashMap;
    }

    public HashMap<String, Integer> getHashMap() { return hashMap; }

    public LinkedHashSet<String> getLinkedHashSet() {
        return linkedHashSet;
    }

    public ArrayList<String> getArrayList() {
        return arrayList;
    }
}
