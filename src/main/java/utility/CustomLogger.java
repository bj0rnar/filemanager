package utility;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CustomLogger {

    /**
     * Singleton Logger class for writing to the Log.txt file
     *
     */

    private final String logPath = "Log.txt";
    private BufferedWriter bufferedWriter;
    private static CustomLogger customLogger = null;

    //Private constructor for singleton pattern. Creates a single instance of BufferedWriter for use
    private CustomLogger(){
        try {
            FileWriter f = new FileWriter(logPath, true);
            bufferedWriter = new BufferedWriter(f);
        }
        catch (IOException e){
            System.out.println(e.getMessage());
        }

    }

    //Get the only instance of the logger. Synchronized so it's thread safe
    public static synchronized CustomLogger getInstance(){
        if (customLogger == null){
            customLogger = new CustomLogger();
        }
        return customLogger;
    }

    //Used for logging action. Flushed so entries will be written despite ternimation
    public void logUserAction(String action, long executionTime){
        String entry = StringFormatter.getCurrentTimeInLoggableFormat() + " User " + action  + ". It took " + executionTime + " ns to execute\n";
        try {
            bufferedWriter.write(entry);
            bufferedWriter.flush();
        }
        catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    //Used for logging program errors, errormessage consists of relevant exceptions getMessage() method
    public void logProgramError(String errorMessage){
        String entry = StringFormatter.getCurrentTimeInLoggableFormat() + errorMessage;
        try {
            bufferedWriter.write(entry);
            bufferedWriter.flush();
        }
        catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    //Close the stream on termination of main loop.
    public void cleanUp(){
        try {
            bufferedWriter.close();
        }
        catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

}
