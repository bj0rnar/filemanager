package utility;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class StringFormatter {

    //Using SimpleDateFormat and Date to format the time for logging purposes.
    public static String getCurrentTimeInLoggableFormat(){
        SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        return timeFormatter.format(date);
    }
}
