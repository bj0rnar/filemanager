package utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;

public class ListSearcher {

    /**
     * Class for searching through given lists.
     */

    //Using Integer instead of int here as Hashmap.get methods returns null and not 0 when get is not found.
    public static Integer frequencyHashMap(HashMap<String, Integer> map, String searchWord){
        return map.get(searchWord);
    }

    //just iterate through the list in a normal fashion
    public static int frequencyNaive(ArrayList<String> list, String searchWord){
        int counter = 0;
        for (String s : list){
            if (s.equals(searchWord)){
                counter++;
            }
        }
        return counter;
    }

    //LinkedHashSet scored the highest out of all tried methods (binary search on sorted set, BST, hashset, etc). It's super quick
    public static boolean unsortedLinkedHashSetSearch(LinkedHashSet<String> set, String searchword){
        return set.contains(searchword);
    }

    //Just itearting through the entire thing again
    public static boolean naiveWordSearch(ArrayList<String> words, String searchword) {

        for (String word : words){
            if (word.equals(searchword)){
                return true;
            }
        }
        return false;

    }
}
