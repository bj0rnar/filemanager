package inputvalidation;

public class MenuInputValidator {

    /**
     * Class for holding the methods used for input validation in the program loop.
     */

    public static boolean checkIfUserEnteredValidMenuOption(String userInput, int numberOfValidOptions){

        //Check if entry is empty
        if (checkIfEntryIsEmpty(userInput)){
            return false;
        }

        //Length of input is more than 1 character
        if (userInput.length() != 1){
            return false;
        }

        //Checks if input is not a digit
        if (!userInput.matches("\\d+")){
            return false;
        }

        //Now safe to parse, check if the input is within the bounds of valid options presented
        if (Integer.parseInt(userInput) > numberOfValidOptions || Integer.parseInt(userInput) <= 0){
            return false;
        }

        return true;
    }

    public static boolean checkIfSearchWordIsValid(String userInput){

        //Empty check
        if (checkIfEntryIsEmpty(userInput)){
            return false;
        }

        //Checks if input consist only of letters
        if (userInput.matches("[^a-zæøåA-ZÆØÅ]")){
            return false;
        }

        //Minimum 2 letter searches and "Pneumonoultramicroscopicsilicovolcanoconiosis" which is the longest english word.
        if (userInput.length() < 1 || userInput.length() > 45){
            return false;
        }

        return true;
    }



    //Checks if the input consist only of blank spaces
    private static boolean checkIfEntryIsEmpty(String userInput) {
        return userInput.replaceAll("\\s+", "").equals("");
    }


}
