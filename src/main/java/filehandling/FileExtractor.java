package filehandling;

import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.ArrayList;

public class FileExtractor {
    /**
     * Singleton class for extracting files from the resources folder
     */


    private final String folderPath = "src/main/resources";
    private static FileExtractor fileExtractor = null;
    private final File resourcesDirectory;

    //private Constructor
    private FileExtractor(){
        resourcesDirectory = new File(folderPath);
    }

    //Singleton pattern, making sure there's only one instance of FileExtractor
    public static FileExtractor getInstance(){
        if (fileExtractor == null){
            fileExtractor = new FileExtractor();
        }
        return fileExtractor;
    }

    //Retrieves file from string search
    public File getSpecificFileByName(String nameOfFile)  {
        File[] allFiles = resourcesDirectory.listFiles();
        for (File entry : allFiles) {
            if (entry.getName().equals(nameOfFile)) {
                    return entry;
            }
        }
        return null;
    }

    //Returns all the files
    public File[] getAllFiles(){
        return resourcesDirectory.listFiles();
    }

    //Iterates through all files in resources directory. Using filenameutils library to cover edge cases like tar.gz
    public ArrayList<File> getFilesByExtension(String desiredExtension){
        File[] fileArray = resourcesDirectory.listFiles();
        ArrayList<File> retrievedFiles = new ArrayList<>();
        for (File file : fileArray){
            if (FilenameUtils.getExtension(file.getName()).equals(desiredExtension)){
                retrievedFiles.add(file);
            }
        }
        return retrievedFiles;
    }
}
