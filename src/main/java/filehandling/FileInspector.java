package filehandling;

import utility.CustomLogger;

import java.io.*;

public class FileInspector {
    /**
     * Class for pure File object operations
     */


    //returns the toStringed version of the file name
    public static String retrieveNameOfFile(File file){
        return file.getName();
    }

    //Uses bufferedReader to count the number of lines in file. returns the incremented value
    public static int countNumberOfLinesInFile(File file){
        int lineCounter = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(file))){
            while (reader.readLine() != null){
                lineCounter++;
            }
            return lineCounter;
        }
        catch (FileNotFoundException fileNotFoundException){
            System.out.println("Cannot find " + file.getName() + " in the resource folder, you sure it's there?");
            CustomLogger.getInstance().logProgramError(fileNotFoundException.getMessage());
        }
        catch (IOException ioException){
            System.out.println("Cannot read lines from " + file.getName());
            CustomLogger.getInstance().logProgramError(ioException.getMessage());
        }
        return lineCounter;
    }

    //Returns the file size in bits
    public static long checkFileSize(File file){
        return file.length();
    }


}
