package init;

import model.Sets;

import java.io.File;
import java.util.*;

public class Initializer {

    /**
     * Class used for formatting the Dracula.txt text for optimized and unoptimized word searches/frequency counts
     * Methods are called upon program start
     */

    //Iterates through the entire text and adds all words in lowercase without special characters.
    //Method returns a custom object for holding both an arraylist and a linkedhashset
    public static Sets searchForWordInit(){

        try {
            ArrayList<String> allWords = new ArrayList<>();
            LinkedHashSet<String> linkedHashSet = new LinkedHashSet<>();
            Scanner sc = new Scanner(new File("src/main/resources/Dracula.txt"));
            while (sc.hasNext()){
                String s = sc.next().toLowerCase().replaceAll("[^a-zA-Z]", "");
                //ignore entries consisting of only special characters or numbers
                if (!s.equals("")) {
                    allWords.add(s);
                    linkedHashSet.add(s);
                }
            }
            sc.close();

            //Create custom object with both lists using the first constructor of the Sets class.
            return new Sets(allWords, linkedHashSet);

        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return null;
    }

    //Same procedure as the method over
    public static Sets wordFrequencyInit(){
        try {
            ArrayList<String> allWords = new ArrayList<>();
            HashMap<String, Integer> map = new HashMap<>();

            Scanner sc = new Scanner(new File("src/main/resources/Dracula.txt"));
            while (sc.hasNext()){
                String s = sc.next().toLowerCase().replaceAll("[^a-zA-Z]", "").toLowerCase();
                if (!s.equals("")) {
                    allWords.add(s);
                    //Hashmap entries are <String,Integer> so we increment the Integer if the hashmap contains the key.
                    //This way each entry holds unique words and the number of times they appear
                    if (map.containsKey(s)) {
                        map.put(s, map.get(s) + 1);
                    } else {
                        map.put(s, 1);
                    }
                }
            }

            //Using the second constructor of the Sets class to hold arraylist and hashmap
            return new Sets(allWords, map);

        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }

        return null;
    }
}
